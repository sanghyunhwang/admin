# README #

admin 최초 commit 

## Project ##

- admin 관리자 페이지 개발

## Member & Role (Github ID)

- developer
        - 황상현 (sanghyun5468@gmail.com)

## commit history ##

### 2017-01-04 
    - 최초 commit
    - login session 기능 추가 (express , mongodb) 
    - logging 추가 (winston)

### 2017-01-05
    - angular routing 추가

### 2017-01-06
    - 날짜별 log 추가
    - client access log 추가 (method,url,request ip)
    - angular routing 추가
