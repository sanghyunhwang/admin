"use strict";
var mongojs = require('mongojs');
var express = require('express');
var db = mongojs(MONGO_JS_HOST);

var KcpUtil = {};

KcpUtil.isAccessiblePage = (req , next) =>{
    var url =  req.url;
    logger.debug("url : " + url);
    var usrid = req.session.usrid;
    db.uhAuthPage.findOne({url: url}, function(err, task){
        if(err){
            logger.error(err);
        }
        logger.debug("task : " + task);
        next(task);
    });
}

module.exports = KcpUtil;