"use strict";

var winston = require('winston');
require('date-utils');

var define = function(name, value) {
  Object.defineProperty(global, name, {
    value : value,
    enumerable : true,
    writable:     false,
    configurable: false
  })
}
var log_path = './logs/';
var max_log_file_size = 1000000;
var logType = process.env.ENV.trim() === 'development' ? 'debug' : 'info';
console.log("logType : " + logType);
winston.configure({
    level : logType,
    handleExceptions : [
        new (require('winston-daily-rotate-file'))({
          name: 'errorLog',
          level:'error',
          filename: log_path + 'err.log',
          maxsize: max_log_file_size,
          maxFiles:5,
          timestamp: function(){return new Date().toFormat('YYYY-MM-DD HH24:MI:SS')},
          json:false
        })
    ],
    transports: [
      new (winston.transports.Console)({
        timestamp: function(){return new Date().toFormat('YYYY-MM-DD HH24:MI:SS')},
      }),
      new (require('winston-daily-rotate-file'))({
        name: logType + 'Log',
        level: logType,
        filename:  log_path + logType + '.log',
        maxsize: max_log_file_size,
        maxFiles:5,
        timestamp: function(){return new Date().toFormat('YYYY-MM-DD HH24:MI:SS')},
        json:false,
      }),                
    ]
  });

<<<<<<< HEAD
var myCustomLevels = {
    levels: {
      aLog: 99,
    }
};  

// server access 로그
var customLevelLogger = new (winston.Logger)({ 
  levels: myCustomLevels.levels 
  ,transports: [
    new (require('winston-daily-rotate-file'))({
      level : 'access',
      filename:  log_path + 'access.log',
      maxsize: max_log_file_size,
      maxFiles:5,
      timestamp: function(){return new Date().toFormat('YYYY-MM-DD HH24:MI:SS')},
      json:false,
    }),                
  ]
});
  

=======
>>>>>>> 0fa0cfef86be06cfdf2a4e84d4f779dde93f028f
define('MONGO_JS_HOST', 'mongodb://test:1234@127.0.0.1:27017/kpcadmin');
define('MONGO_JS_HOST_SESSION', 'mongodb://session_admin:1234@127.0.0.1:27017/session');
define('MONGO_HOST', '127.0.0.1');
define('MONGO_DB', 'kpcadmin');
define('MONGO_SESSION_DB', 'session');
define('MONGO_PORT', '27017');
define('MONGO_USER', 'test');
define('MONGO_PW', '1234');
define('MONGO_COLLECTION', 'tasks');
define('MONGO_COLLECTION_SESSION', 'session');
define('APP_SERVER_PORT', '3000');
define('CONTROLLER_PATH', './controllers/');
define('KPC_SESSION_SECRET_KEY', 'test');
define('LOG_PATH', log_path);
define('logger', winston);
define('accessLogger', customLevelLogger);

