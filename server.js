var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();
var router = express.Router();
var helmet = require('helmet');
var MongoStore = require('connect-mongo')(session);
// global variable load
require('./config/system'); 
var KcpUtil = require('./libraries/KcpUtil'); 

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// 취약성 예방 코드  
// https://www.npmjs.com/package/helmet 참조
app.use(helmet());

// session set
app.use(cookieParser());
// mongodb 저장된 session 값에 대해서 운영중 서버가 restart 되면 관련 session 기록정보가 남아 있게 됨
// 서버 재시작시 관련 db데이터를 다 삭제 하고 다시 시작할지 확인필요..
app.use(session({ 
    secret: KPC_SESSION_SECRET_KEY, // 비밀키 
    store : new MongoStore({
        url : MONGO_JS_HOST_SESSION,
        collection : MONGO_COLLECTION_SESSION,
        autoRemove : 'interval',        
        autoRemoveInterval: 3 // In minutes. Default 
    }),
    cookie: {
        maxAge: 1000 * 60 * 3, // 쿠키 유효기간 3분
        secure : true
    }, 
    resave: true,
    saveUninitialized: true
})); 

// middleware setting url get 요청을 파싱
// 잘못된 방식 angular 에서 체크 추가 
// router.get('/' , function (req, res, next) {
//     // 비로그인 페이지 설정
//     logger.debug("req.url : " + req.url);
//     // db내의 비로그인 페이지 검사 비로그인 페이지가 아니라면 로그인 페이지로 redirect
//     KcpUtil.isAccessiblePage(req,(data)=>{
//         if(data === null || typeof req.session.usrid === "undefined"){
//             res.redirect('/login');
//         }
//         next(); 
//     });

// });
<<<<<<< HEAD
// access log 기록
router.use(function(req, res, next) {
  accessLogger.aLog('method=[%s], url=[%s], remoteAddress=[%s]', req.method, req.url, req.connection.remoteAddress);
  next();
});
app.use('/', router);
=======
//app.use('/', router);
>>>>>>> 0fa0cfef86be06cfdf2a4e84d4f779dde93f028f
// controller route setting
(function initController(filePath , requirePath){
    fs.readdir(filePath, (err, files) => {
        files.forEach(file => {
            var innerDirPath = path.join( filePath ,file);
            if(fs.lstatSync(innerDirPath).isDirectory()){
                initController(innerDirPath , requirePath + file + "/");
            }else{
                var ext = path.extname(file);
                if ( ext === '.js') {
                    logger.debug("requirePath + file : " + requirePath + file);
                    app.use('/', require(requirePath + file));
                }
            }
        });
    });
}(path.join( __dirname, 'controllers' ) , CONTROLLER_PATH));

// var index = require('./routes/index');
// var tasks = require('./routes/tasks');

// app.use('/', index);
// app.use('/api', tasks);

process.on('uncaughtException', (err) => {
    logger.error(err);
});

app.listen(APP_SERVER_PORT, function(){
    logger.info('Server started on port '+APP_SERVER_PORT);
}); // end listen