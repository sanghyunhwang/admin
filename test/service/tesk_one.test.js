var assert = require('assert'),
    http = require('http');

describe('/', function () {
  it('should return 200', function (done) {
    http.get('http://localhost:3000/api/task/586de80ce53a1b16f49c3124', function (res) {
      assert.equal(200, res.statusCode);
      done();
    });
  });

  it('get one task', function (done) {
    http.get('http://localhost:3000/api/task/586de80ce53a1b16f49c3124', function (res) {
      var data = '';

      res.on('data', function (chunk) {
        data += chunk;
      });

      res.on('end', function () {               
        var json_data = JSON.parse(data);
        assert.equal("586de80ce53a1b16f49c3124", json_data._id);
        done();
      });
    });
  });
});