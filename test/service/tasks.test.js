var assert = require('assert'),
    http = require('http');

describe('/', function () {
  it('should return 200', function (done) {
    http.get('http://localhost:3000/api/tasks', function (res) {
      assert.equal(200, res.statusCode);
      done();
    });
  });

  it('get JSON data', function (done) {
    http.get('http://localhost:3000/api/tasks', function (res) {
      var data = '';

      res.on('data', function (chunk) {
        data += chunk;
      });

      res.on('end', function () {               
        assert.equal("object", typeof JSON.parse(data));
        done();
      });
    });
  });
});