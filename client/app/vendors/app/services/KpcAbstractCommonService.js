/**
 * KpcCommonService
 * @author sanghyun
 * @since 2017.01.05
 */
var KpcAbstractCommonService = (function () {
    function KpcAbstractCommonService() {
    }
    KpcAbstractCommonService.prototype.delete = function (param) {
        return null;
    };
    KpcAbstractCommonService.prototype.insert = function (param) {
        return null;
    };
    KpcAbstractCommonService.prototype.update = function (param) {
        return null;
    };
    KpcAbstractCommonService.prototype.select = function (param) {
        return null;
    };
    return KpcAbstractCommonService;
}());
//# sourceMappingURL=KpcAbstractCommonService.js.map