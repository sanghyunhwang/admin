"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('./components/common/login/login.component');
var tasks_component_1 = require('./components/tasks/tasks.component');
var main_component_1 = require('./components/layout/main/main.component');
// Config
var hashLocationStrategy = false;
var commonRoutes = [
    { path: '', component: main_component_1.MainComponent },
    { path: 'task', component: tasks_component_1.TasksComponent },
    { path: 'login', component: login_component_1.LoginComponent },
];
var layoutRoutes = [
    { path: 'main', component: main_component_1.MainComponent },
];
var appRoutes = commonRoutes.concat(layoutRoutes);
exports.AppRoutingModule = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map