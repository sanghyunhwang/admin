import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { LoginService } from './../../services/common/login/login.service';
import { MainComponent } from './main/main.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
  ],
  declarations: [
    MainComponent,
  ],
  providers: [
    LoginService
  ],
})
export class LayoutModule { }
