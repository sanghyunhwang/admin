import { Component } from '@angular/core';
import { LoginService } from '../../../services/common/login/login.service';
/**
 * login component
 * @author sanghyun
 * @since 2017.01.05
 */
@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: 'login.component.html',
})
export class LoginComponent {
    constructor(private loginService:LoginService){
      console.log("call LoginComponent!");
    }  
}