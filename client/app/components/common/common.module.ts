import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { LoginComponent } from './login/login.component';
import { LoginService } from '../../services/common/login/login.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
  ],
  declarations: [
    LoginComponent,
  ],
  providers: [
    LoginService
  ],
})
export class CommonModule { }
