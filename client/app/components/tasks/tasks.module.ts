import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { TasksComponent } from './tasks.component';
import { TaskService } from '../../services/tasks/task.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
  ],
  declarations: [
    TasksComponent,
  ],
  providers: [
    TaskService
  ],
})
export class TasksModule { }
