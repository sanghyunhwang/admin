interface CommonService{
    select(param : Map<string,string>) : any;
    update(param : Map<string,string>) : any;
    insert(param : Map<string,string>) : any;
    delete(param : Map<string,string>) : any;
    
}