/**
 * KpcCommonService
 * @author sanghyun
 * @since 2017.01.05
 */
abstract class KpcAbstractCommonService implements CommonService{

    public delete(param : Map<string,string>): any {
        return null;
    }

    public insert(param : Map<string,string>): any {
        return null;
    }

    public update(param : Map<string,string>): any {
        return null;
    }

    public select(param : Map<string,string>): any {
        return null;
    }
}